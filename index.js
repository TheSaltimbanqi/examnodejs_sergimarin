var http = require('http');
var fileType = require('file-type');
var express=require("express"),
    bodyParser = require('body-parser');

var app = express();

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
// parse application/json
app.use(bodyParser.json());

//var act2={palabras:'hola,como,estas,hola,como,estas,hola,como,estas'};

//Especificacion 2
app.post('/removeduplicatewords', function(req, res)
{
  var words=req.body.palabras;
  console.log(words);


  //var array=test.split(",");
  var array=words.split(",");

  var words=[];
  for(var i=0;i<array.length;++i)
  {
    if(!words.includes(array[i]))
    {
      words.push(array[i]);
      console.log(array[i]);
    }
  }
  var result="";
  for(var i=0;i<words.length-1;++i)
  {
    result+=words[i]+",";
  }
  result+=words[words.length-1];
  //var result=words[0]+","+words[1]+","+words[2];
  res.end(result);
});

//Especificacion 3

//var jsondft = { "url": "http://www.jandusoft.com/images/JanduSoft_logo.png"};
app.post('/detectfiletype', function(req, res)
{
  var url=req.body.url;
  var result1;
  http.get(url, result => {
      result.once('data', chunk => {
          result.destroy();
          var result2=(fileType(chunk));
          result1=JSON.stringify(result2);
          console.log(result1);
          //=> {ext: 'gif', mime: 'image/gif'}
          res.end(result1);
      });
  });
  console.log(url);
});

//Especificacion 4

app.listen(8000);
